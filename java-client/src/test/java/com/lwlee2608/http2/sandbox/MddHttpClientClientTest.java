package com.lwlee2608.http2.sandbox;

import com.matrixx.mdd.core.MddDatabase;
import com.matrixx.mdd.generated.MtxRequestPricingQueryStatus;
import com.matrixx.mdd.generated.MtxResponsePricingStatus;
import com.matrixx.mdd.transport.TransportType;
import com.matrixx.mdd.transport.client.MddClientConfig;
import com.matrixx.mdd.transport.http.vertx.reactivex.client.MddHttpClient;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import io.vertx.reactivex.core.Vertx;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(VertxExtension.class)
public class MddHttpClientClientTest {
    private static final Logger logger = LoggerFactory.getLogger(MddHttpClientClientTest.class);

    @BeforeAll
    void init() {
        MddDatabase.scan();
    }

    @Test
    @Timeout(value = 50000 , unit = TimeUnit.MILLISECONDS)
    void testServerClientWithRoute(Vertx vertx, VertxTestContext testContext) throws Exception {
        Checkpoint responseReceived = testContext.checkpoint(2);
        int port = 8080;

        // Client
        MddHttpClient mdcClient = new MddHttpClient(vertx, new MddClientConfig().setTransportType(TransportType.HTTP_2));
        Completable clientConnect = Completable.defer(() -> mdcClient.connect("localhost", port));

        // Fire request
        MtxRequestPricingQueryStatus request = new MtxRequestPricingQueryStatus();
        request.setFlags(123);
        Single<MtxResponsePricingStatus> c1 = Single.defer(() -> mdcClient.sendMtxRequest("ROUTE",
                        new MtxRequestPricingQueryStatus().setClientSessionId("1"))
                .map(response -> (MtxResponsePricingStatus) response)
                .doOnSuccess(response -> {
                    Assertions.assertEquals(response.getResult(), 0L);
                    Assertions.assertEquals(response.getResultText(), "Success");
                    responseReceived.flag();
                }));

        Single<MtxResponsePricingStatus> c2 = Single.defer(() -> mdcClient.sendMtxRequest("ROUTE",
                        new MtxRequestPricingQueryStatus().setClientSessionId("2"))
                .map(response -> (MtxResponsePricingStatus) response)
                .doOnSuccess(response -> {
                    Assertions.assertEquals(response.getResult(), 0L);
                    Assertions.assertEquals(response.getResultText(), "Success");
                    responseReceived.flag();
                }));

        clientConnect
                .andThen(Single.zip(c1, c2, (r1, r2) -> r1).ignoreElement())
                .andThen(Completable.defer(mdcClient::disconnect))
                .subscribe(() -> {}, testContext::failNow);
    }
}
