# HTTP2 Server

## Prerequisite Tools
* GCC
* CMake
* Makefile

## Prerequisite Libraries

### Thirdparty dependencies

Boost and ssl are required to build nghttp2. Install it via the following command (only work with debian)
```bash
 sudo apt install libboost-thread-dev
 sudo apt-get install libssl-dev
```

### Compile and Install nghttp2
The following commands will checkout nghttp2 source code, compile and install to your machine.
```bash
git clone https://github.com/nghttp2/nghttp2.git 
mkdir build
cd build
cmake -DENABLE_ASIO_LIB=ON ..
make
sudo make install
```

## Building the applications
```bash
mkdir build
cd build
cmake ..
make
```

## Running the application
Start server on one terminal
```bash
./http2-server
```

Start client on another terminal
```bash
./http2-client
```

## Test with curl
```bash
curl -v --http2-prior-knowledge localhost:8080 -d '<1,5,0,238,5222,1>[,<1,17,0,244,5222,1>[,,(7:Request)]]'
```