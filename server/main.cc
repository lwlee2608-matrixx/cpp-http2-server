#include <boost/thread/thread.hpp>
#include <iostream>
#include <nghttp2/asio_http2_server.h>
#include "ctpl.h"

using namespace nghttp2::asio_http2;
using namespace nghttp2::asio_http2::server;

std::atomic<int> i(0);

int main(int argc, char *argv[]) {
    boost::system::error_code ec;
    http2 server;
    auto host = "0.0.0.0";
    auto port = "8080";

    ctpl::thread_pool workers(4);

    server.num_threads(4);
    server.handle("/", [&workers](const request &req, const response &res) {
        req.on_data([&res, &workers](const uint8_t *data, std::size_t len) {
            if (len != 0) {
                // Process the request in a separate threadpool
                workers.push([&res](int id, const std::string &mdcReq) {
                        std::cout << "received : " << mdcReq << std::endl;

                        // Fake response MDC
                        std::ostringstream ss;
                        ss << "<1,5,0,238,5222,1>[,<1,17,0,244,5222,1>[,,(7:""Response" << ++i << ")]]";
                        auto mdcRes = ss.str();

                        // Valid response MDC
                        // auto mdcRes =
                        // "<1,5,0,238,5222,1>[,,<1,13,0,379,5222,1>[]<1,4,0,236,5222,1>[,0,(7:Success)]]<1,18,0,94,5222,1>[]<1,13,0,411,5222,1>[]<1,24,0,93,5222,1>[,,,,,,,,,,,,,,1]";

                        // Simulate processing delay of 1 second
                        sleep(1);

                        std::cout << "responding : " << mdcRes << std::endl;
                        res.write_head(200);
                        res.end(mdcRes);
                    },
                    std::string(reinterpret_cast<const char *>(data), len));
            }
        });
    });

    std::cout << "listening on " << host << ":" << port << std::endl;
    if (server.listen_and_serve(ec, host, port)) {
        std::cerr << "error: " << ec.message() << std::endl;
    }
}