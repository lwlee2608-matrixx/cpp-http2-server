#include <iostream>
#include <nghttp2/asio_http2_client.h>
using boost::asio::ip::tcp;

using namespace nghttp2::asio_http2;
using namespace nghttp2::asio_http2::client;

int main(int argc, char *argv[]) {
    boost::system::error_code ec;
    boost::asio::io_service io_service;

    session sess(io_service, "localhost", "8080");
    sess.on_connect([&sess](tcp::resolver::iterator endpoint_it) {
        boost::system::error_code ec;

        nghttp2::asio_http2::header_map headers;
        headers.insert(std::pair<std::string, header_value>("route",{"RT1D1"}));

        auto printer = [](const response &res) {
            res.on_data([](const uint8_t *data, std::size_t len) {
                if (len != 0) {
                    std::string mdcRes = std::string(reinterpret_cast<const char *>(data), len);
                    std::cout << "received : " << mdcRes << std::endl;
                }
            });
        };

        std::size_t num = 2;
        auto count = std::make_shared<int>(num);
        for (std::size_t i = 0; i < num; ++i) {
            std::ostringstream ss;
            ss << "<1,5,0,238,5222,1>[,<1,17,0,244,5222,1>[,,(7:Request" << i+1 << ")]]";
            auto mdcReq = ss.str();
            std::cout << "sending : " << mdcReq << std::endl;

            auto req = sess.submit(ec, "POST", "http://localhost:8080/", mdcReq, headers);
            req->on_response(printer);
            req->on_close([&sess, count](uint32_t error_code) {
                if (--*count == 0) {
                    sess.shutdown();
                } 
            });
        }
    });

    sess.on_error([](const boost::system::error_code &ec)
                  { std::cerr << "error: " << ec.message() << std::endl; });

    io_service.run();
}